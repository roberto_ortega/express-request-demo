var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var groceryRoutes = require('./routes/groceries')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/', function (request, response) {
  response.send('hello!!')
})

app.use('/', groceryRoutes)

app.listen(5000)
