function getGroceryById(groceries, id) {
  for (var i = 0; i < groceries.length; i++) {
    if (groceries[i].id == id) {
      return groceries[i]
    }
  }
}

function filterGroceriesByType(groceries, type) {
  var newList = []
  for (var i = 0; i < groceries.length; i++) {
    if (groceries[i].type === type) {
      newList.push(groceries[i])
    }
  }
  return newList
}

module.exports = {
  getGroceryById: getGroceryById,
  filterGroceriesByType: filterGroceriesByType
}
