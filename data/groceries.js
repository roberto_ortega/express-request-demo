module.exports = [
  {
    id: 0,
    type: "vegetable",
    name: "broccoli"
  },
  {
    id: 1,
    type: "vegetable",
    name: "onions"
  },
  {
    id: 2,
    type: "vegetable",
    name: "carrots"
  },
  {
    id: 3,
    type: "fruit",
    name: "watermelon"
  },
  {
    id: 4,
    type: "snack",
    name: "chips"
  }
]
