var express = require('express')
var router = express.Router()
var groceries = require('../data/groceries')
var filterGroceriesByType = require('../logic').filterGroceriesByType
var getGroceryById = require('../logic').getGroceryById

router.get('/groceries', function (request, response) {
  var type = request.query.type
  var filteredGroceries = groceries
  if (type) {
    filteredGroceries = filterGroceriesByType(groceries, type)
  }
  response.json(filteredGroceries)
})

router.post('/groceries', function (request, response) {
  var item = request.body
  item.id = groceries.length
  groceries.push(item)
  response.send(`Yay!!! your item was added to the list!!`)
})

router.get('/groceries/:itemId', function (request, response) {
  var id = request.params.itemId
  var groceryItem = getGroceryById(groceries, id)
  response.json(groceryItem)
})

router.get('/groceries/:type/:id', function (request, response) {
  response.json(request.params)
})

module.exports = router
